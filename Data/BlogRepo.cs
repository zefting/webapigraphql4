using WebApiGraphQl4.Models;
using System.Collections.Generic;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiGraphQl4.Data
{
    public class BlogRepo : IBlogRepo
    {
        private readonly postgresContext _context;
        public BlogRepo(postgresContext context)
        {
            _context = context;
        }
         public bool SaveChanges()
        {
            return(_context.SaveChanges() >=0);
        }
        public List<Author> GetAllAuthors()
        {
            return _context.Authors.ToList();
        }
        public List<Blogpost> GetAllBlogposts()
        {
            return _context.Blogposts.ToList();
        }
        public Blogpost GetBlogPostById(int id)
        {
            return _context.Blogposts.Where(Blogpost => Blogpost.BlogpostId == 
            id).FirstOrDefault<Blogpost>();
        }
        public Blogpost UpdateBlogpost(Blogpost blogpost)
        {
            var postToUpdated = _context.Blogposts.FirstOrDefault(post => post.BlogpostId == blogpost.BlogpostId);
            postToUpdated.Content = blogpost.Content;
            postToUpdated.Title = blogpost.Title;
            postToUpdated.AuthorId = blogpost.AuthorId;
            _context.Update(postToUpdated);
            _context.SaveChanges();
            return postToUpdated;
            
        }
        public void DeleteBlogPost(int id)
        {
            var postToBeDeleted = _context.Blogposts.FirstOrDefault(post => post.BlogpostId == id); 
            _context.Remove(postToBeDeleted);
        }
        public Author GetAuthorById(int id)
        {
            return _context.Authors.Where(author => author.AuthorId == 
            id).FirstOrDefault<Author>();
        }
        public List<Blogpost> GetPostsByAuthor(int id)
        {
            return _context.Blogposts.Where(post => post.Author.AuthorId == 
            id).ToList<Blogpost>();
        }
        public Author AddAuthor (Author author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
            return author; 
        }

        public Blogpost AddBlogPost(Blogpost blogpost)
        {
           
            _context.Blogposts.Add(blogpost);
            _context.SaveChanges();
            return blogpost;
        }
    }
}