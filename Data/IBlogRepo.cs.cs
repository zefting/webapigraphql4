using System.Collections.Generic;
using WebApiGraphQl4.Models;
namespace WebApiGraphQl4.Data
{
    public interface IBlogRepo
    {
        //save Changes
        bool SaveChanges();
        List<Author> GetAllAuthors();
        Author GetAuthorById(int id);    
        Author AddAuthor (Author author);

        Blogpost AddBlogPost(Blogpost blogpost);
        Blogpost GetBlogPostById(int id);
        List<Blogpost> GetPostsByAuthor(int id);
        List<Blogpost> GetAllBlogposts();

        Blogpost UpdateBlogpost(Blogpost blogpost);

        void DeleteBlogPost(int id);

    }
}