using WebApiGraphQl4.Services;

namespace WebApiGraphQl4.Data
{
    public interface ISubscriptionServices
    {
        BlogPostAddedService BlogPostAddedService { get; }
    }
}