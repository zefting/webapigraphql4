using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Npgsql.EntityFrameworkCore;

using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using WebApiGraphQl4.Graph.Schema;
using WebApiGraphQl4.Graph.Query;
using WebApiGraphQl4.Graph.Mutation;
using WebApiGraphQl4.Graph.Subscription;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Services;

using GraphQL;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphiQl;



namespace WebApiGraphQl4
{
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<postgresContext>(options =>
           {
               options.UseNpgsql(Configuration["ConnectionString"]);
           });
            services.AddScoped<IBlogRepo, BlogRepo>();
            services.AddScoped<IFieldService, FieldService>();
            services.AddScoped<IDocumentExecuter, DocumentExecuter>();
            //services.AddScoped<>();
            services.AddScoped<MainMutation>();
            services.AddScoped<MainQuery>();
            services.AddScoped<GraphQLSchema>();
            services.AddScoped<BlogpostGType>();
            services.AddScoped<AuthorGType>();

            services.AddSingleton<ISubscriptionServices, SubscriptionServices>();
            services.AddScoped<MainSubscription>();

            services.AddGraphQL(options =>
            {
                options.EnableMetrics = true;
            })
            .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true)
            .AddSystemTextJson()
            .AddGraphTypes(ServiceLifetime.Scoped)
            .AddWebSockets();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseWebSockets();
            app.UseGraphQLWebSockets<GraphQLSchema>();
            app.UseAuthorization();
            app.UseGraphiQl("/graphql");
            // add http for Schema at default url /graphql
            app.UseGraphQL<GraphQLSchema>();
            // use HTTP middleware for ChatSchema at default path /graphql
            app.UseGraphQL<GraphQLSchema>();
            // use graphql-playground at default url /ui/playground
            app.UseGraphQLPlayground();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
