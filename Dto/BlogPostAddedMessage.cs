using System;
using WebApiGraphQl4.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;

namespace WebApiGraphQl4.Dto
{
    public class BlogPostAddedMessage
    {
        public int id { get; set; }
        public string title { get; set; }
        public string author { get; set; }
        public string content { get; set; }
    }
}