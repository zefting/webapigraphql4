using GraphQL.Types;
using GraphQL;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Linq;

namespace WebApiGraphQl4.Graph.Query.Business
{
    public class AuthorQuery : IFieldQueryServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<ListGraphType<AuthorGType>>("Authors",
               arguments: new QueryArguments(
                 new QueryArgument<StringGraphType> { Name = "name" }
               ),
               resolve: context =>
               {
                   var authorRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));
                   var baseQuery = authorRepository.GetAllAuthors();
                   var name = context.GetArgument<string>("name");
                   if (name != default(string))
                   {
                       return baseQuery.Where(w => w.Firstname == name);
                   }
                   return baseQuery.ToList();
               });
        }
    }
}