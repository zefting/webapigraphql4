using GraphQL.Types;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using System;
using GraphQL;
using System.Linq;

namespace WebApiGraphQl4.Graph.Query.Business
{
    public class BlogpostQuery : IFieldQueryServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<ListGraphType<BlogpostGType>>("Blogposts",
               arguments: new QueryArguments(
                 new QueryArgument<IntGraphType> { Name = "id" }
               ),
               resolve: context =>
               {
                   var blogpostRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));
                   var baseQuery = blogpostRepository.GetAllBlogposts();
                   var id = context.GetArgument<int>("id");
                   if (id != default(int))
                   {
                       return baseQuery.Where(w => w.Author.AuthorId == id);
                   }
                   return baseQuery.ToList();
               });
        }
    }
}