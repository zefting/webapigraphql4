using GraphQL.Types;
using WebApiGraphQl4.Data;
using Microsoft.AspNetCore.Hosting;
using System;

namespace WebApiGraphQl4.Graph.Query
{
    public class MainQuery : ObjectGraphType
    {
        public MainQuery(IServiceProvider provider, IWebHostEnvironment env, IFieldService fieldService)
        {
            Name = "MainQuery";
            fieldService.ActivateFields(this, FieldServiceType.Query, env, provider);
        }
    }
}