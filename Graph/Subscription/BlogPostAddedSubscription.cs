using GraphQL.Resolvers;
using GraphQL.Types;
using WebApiGraphQl4.Dto;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using Microsoft.AspNetCore.Hosting;
using GraphQL;
using System;

namespace WebApiGraphQl4.Graph.Subscription
{
    public class BlogPostAddedSubscription : IFieldSubscriptionServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.AddField(new EventStreamFieldType
            {
                Name = "BlogpostAdded",
                Type = typeof(BlogPostAddedMessageGType),
                Resolver = new FuncFieldResolver<BlogPostAddedMessage>(context => context.Source as BlogPostAddedMessage),
                Arguments = new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "author" }
                ),
                Subscriber = new EventStreamResolver<BlogPostAddedMessage>(context =>
                {
                    var subscriptionServices = (ISubscriptionServices)sp.GetService(typeof(ISubscriptionServices));
                    var author = context.GetArgument<string>("author");
                    return subscriptionServices.BlogPostAddedService.GetMessages(author);
                })
            });
        }
    }
}