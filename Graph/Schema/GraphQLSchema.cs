using WebApiGraphQl4.Graph.Mutation;
using WebApiGraphQl4.Graph.Query;
using WebApiGraphQl4.Graph.Subscription;
using WebApiGraphQl4.Data;
using GraphQL;
using GraphQL.Types;
using System;
using GraphQL.Utilities;

namespace WebApiGraphQl4.Graph.Schema
{
    public class GraphQLSchema : GraphQL.Types.Schema
    {
        public GraphQLSchema(IServiceProvider provider) : base(provider)
        {
            var fieldService = provider.GetRequiredService<IFieldService>();
            fieldService.RegisterFields();
            Mutation = provider.GetRequiredService<MainMutation>();
            Query = provider.GetRequiredService<MainQuery>();
            Subscription = provider.GetRequiredService<MainSubscription>();
        }
    }
}