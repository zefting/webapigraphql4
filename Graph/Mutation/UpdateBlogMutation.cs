using GraphQL.Types;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using GraphQL;
using System;

namespace WebApiGraphQl4.Graph.Mutation
{
    public class UpdateCityMutation : IFieldMutationServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<BlogpostGType>("updateBlogpost",
            arguments: new QueryArguments(
               new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "blogpostId" },
               new QueryArgument<StringGraphType> { Name = "title" },
               new QueryArgument<StringGraphType> { Name = "content" },
               new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "authorId" }
            ),
            resolve: context =>
            {
                var blogpostId = context.GetArgument<int>("blogpostId");
                var title = context.GetArgument<string>("title");
                var content = context.GetArgument<string>("content");
                var authorId = context.GetArgument<int>("authorId");

                var blogpostRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));
                var foundBlogpost = blogpostRepository.GetBlogPostById(blogpostId);

                foundBlogpost.AuthorId = authorId;

                if (!String.IsNullOrEmpty(content))
                {
                    foundBlogpost.Content = content;
                }

                if (!String.IsNullOrEmpty(title))
                {
                    foundBlogpost.Title = title;
                }

                return blogpostRepository.UpdateBlogpost(foundBlogpost);
            });
        }
    }
}