using GraphQL.Types;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using GraphQL;
using System;


namespace WebApiGraphQl4.Graph.Mutation
{
    public class AddAuthorMutation : IFieldMutationServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<AuthorGType>("addAuthor",
            arguments: new QueryArguments(
                new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "firstName" },
                new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "lastName" }
            ),
            resolve: context =>
            {
                var firstName = context.GetArgument<string>("firstName");
                var lastName = context.GetArgument<string>("lastName");
                var authorRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));

                var newAuthor = new Author();
                {
                    newAuthor.Firstname = firstName;
                    newAuthor.Lastname = lastName;

                };
                return authorRepository.AddAuthor(newAuthor);
            });
        }
    }
}