using GraphQL.Types;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using GraphQL;
using System;

namespace WebApiGraphQl4.Graph.Mutation
{
    public class DeleteBlogPostMutation : IFieldMutationServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<StringGraphType>("deleteBlogPost",
            arguments: new QueryArguments(
               new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "blogpostId" }
            ),
            resolve: context =>
            {
                var blogPostId = context.GetArgument<int>("blogpostId");
                var cityRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));
                cityRepository.DeleteBlogPost(blogPostId);
                return $"cityId:{blogPostId} deleted";
            });
        }
    }
}