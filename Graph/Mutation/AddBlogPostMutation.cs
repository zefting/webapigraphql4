using GraphQL.Types;
using WebApiGraphQl4.Dto;
using WebApiGraphQl4.Graph.Type;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using Microsoft.AspNetCore.Hosting;
using GraphQL;
using System;

namespace WebApiGraphQl4.Graph.Mutation
{
    public class AddBlogpostMutation : IFieldMutationServiceItem
    {
        public void Activate(ObjectGraphType objectGraph, IWebHostEnvironment env, IServiceProvider sp)
        {
            objectGraph.Field<BlogpostGType>("addBlogPost",
            arguments: new QueryArguments(
               new QueryArgument<NonNullGraphType<IntGraphType>> { Name = "authorId" },
               new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "title" },
               new QueryArgument<StringGraphType> { Name = "content" }
            ),
            resolve: context =>
            {
                var authorId = context.GetArgument<int>("authorId");
                var title = context.GetArgument<string>("title");
                var content = context.GetArgument<string>("content");

                var subscriptionServices = (ISubscriptionServices)sp.GetService(typeof(ISubscriptionServices));
                var blogpostRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));
                var authorRepository = (IBlogRepo)sp.GetService(typeof(IBlogRepo));

                var foundAuthor = authorRepository.GetAuthorById(authorId);

                var newBlogpost = new Blogpost();
                {
                    newBlogpost.Title = title;
                    newBlogpost.AuthorId = authorId;
                    newBlogpost.Content = content;
                };

                var addedBlogPost = blogpostRepository.AddBlogPost(newBlogpost);
                subscriptionServices.BlogPostAddedService.AddBlogPostAddedMessage(new BlogPostAddedMessage
                {
                    title = addedBlogPost.Title,
                    author = foundAuthor.Firstname,
                    id = addedBlogPost.BlogpostId,
                    content = "New blogpost added"
                });
                return addedBlogPost;

            });
        }
    }
}