using GraphQL.Types;
using WebApiGraphQl4.Data;
using Microsoft.AspNetCore.Hosting;
using System;

namespace WebApiGraphQl4.Graph.Mutation
{
    public class MainMutation : ObjectGraphType
    {
        public MainMutation(IServiceProvider provider, IWebHostEnvironment env, IFieldService fieldService)
        {
            Name = "MainMutation";
            fieldService.ActivateFields(this, FieldServiceType.Mutation, env, provider);
        }
    }
}