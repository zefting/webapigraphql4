using GraphQL.Types;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using System;

namespace WebApiGraphQl4.Graph.Type
{
    public class BlogpostGType : ObjectGraphType<Blogpost>
    {
        public IServiceProvider Provider { get; set; }
        public BlogpostGType(IServiceProvider provider)
        {
            Field(x => x.BlogpostId, type: typeof(IntGraphType));
            Field(x => x.Title, type: typeof(StringGraphType));
            Field(x => x.Content, type: typeof(StringGraphType));
            Field<AuthorGType>("Author", resolve: context =>
            {
                IBlogRepo BlogpostRepository = (IBlogRepo)provider.GetService(typeof(IBlogRepo));
                return BlogpostRepository.GetAuthorById(context.Source.AuthorId);
            });
        }
    }
}
