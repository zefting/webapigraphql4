using GraphQL.Types;
using WebApiGraphQl4.Dto;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using GraphQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiGraphQl4.Graph.Type
{
    public class BlogPostAddedMessageGType : ObjectGraphType<BlogPostAddedMessage>
    {
        public IServiceProvider provider { get; set; }
        public BlogPostAddedMessageGType()
        {
            Field(x => x.id, type: typeof(IntGraphType));
            Field(x => x.content, type: typeof(StringGraphType));
            Field(x => x.title, type: typeof(StringGraphType));
            Field(x => x.author, type: typeof(StringGraphType));
            /*Field<AuthorGType>("Author", resolve: context =>
            {
                IBlogRepo BlogpostRepository = (IBlogRepo)provider.GetService(typeof(IBlogRepo));
                return BlogpostRepository.GetAuthorById();
            }); */
        }
    }
}