using GraphQL.Types;
using WebApiGraphQl4.Data;
using WebApiGraphQl4.Models;
using System;
using System.Linq;

namespace WebApiGraphQl4.Graph.Type
{
    public class AuthorGType : ObjectGraphType<Author>
    {
        public IServiceProvider Provider { get; set; }
        public AuthorGType(IServiceProvider provider)
        {
            Field(x => x.AuthorId, type: typeof(IntGraphType)).Description("Author's Id.");
            Field(x => x.Firstname, type: typeof(StringGraphType)).Description("The Author's fistname");
            Field(x => x.Lastname, type: typeof(StringGraphType)).Description("The Author's Lastname");
            Field<ListGraphType<BlogpostGType>>("Posts", resolve: context =>
            {
                IBlogRepo BlogpostRepository = (IBlogRepo)provider.GetService(typeof(IBlogRepo));
                return BlogpostRepository.GetAllBlogposts().Where(w => w.Author.AuthorId == context.Source.AuthorId);
            });
        }
    }
}