using WebApiGraphQl4.Data;

namespace WebApiGraphQl4.Services
{
    public class SubscriptionServices : ISubscriptionServices
    {
        public SubscriptionServices()
        {
            this.BlogPostAddedService = new BlogPostAddedService();
        }
        public BlogPostAddedService BlogPostAddedService { get; }
    }
}