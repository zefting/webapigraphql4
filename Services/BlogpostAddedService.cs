using WebApiGraphQl4.Dto;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace WebApiGraphQl4.Services
{
    public class BlogPostAddedService
    {
        private readonly ISubject<BlogPostAddedMessage> _messageStream = new ReplaySubject<BlogPostAddedMessage>(1);
        public BlogPostAddedMessage AddBlogPostAddedMessage(BlogPostAddedMessage message)
        {
            _messageStream.OnNext(message);
            return message;
        }

        public IObservable<BlogPostAddedMessage> GetMessages(string Firstname)
        {
            var mess = _messageStream
                .Where(message =>
                    message.author == Firstname
                ).Select(s => s)
                .AsObservable();
            return mess;
        }
    }
}