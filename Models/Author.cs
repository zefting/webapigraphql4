﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiGraphQl4.Models
{
    public partial class Author
    {
        public Author()
        {
            Blogposts = new HashSet<Blogpost>();
        }

        public int AuthorId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public virtual ICollection<Blogpost> Blogposts { get; set; }
    }
}
