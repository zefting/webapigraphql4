﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiGraphQl4.Models
{
    public partial class Blogpost
    {
        public int BlogpostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }
    }
}
